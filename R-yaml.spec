%global packname yaml
%global packver  2.3.8
%global rlibdir  %{_libdir}/R/library

Name:             R-%{packname}
Version:          2.3.8
Release:          1
Summary:          Methods to Convert R Data to YAML and Back
License:          BSD and MIT
URL:              https://CRAN.R-project.org/package=%{packname}
Source0:          https://cran.r-project.org/src/contrib/%{packname}_%{packver}.tar.gz
BuildRequires:    R-devel
BuildRequires:    tex(latex)
BuildRequires:    R-RUnit
Provides:         bundled(libyaml) = 0.2.5

%description
Implements the 'libyaml' 'YAML' 1.1 parser and emitter
(<http://pyyaml.org/wiki/LibYAML>) for R.


%prep
%setup -q -c -n %{packname}


%build


%install
mkdir -p %{buildroot}%{rlibdir}
%{_bindir}/R CMD INSTALL -l %{buildroot}%{rlibdir} %{packname}
test -d %{packname}/src && (cd %{packname}/src; rm -f *.o *.so)
rm -f %{buildroot}%{rlibdir}/R.css
rm %{buildroot}%{rlibdir}/%{packname}/implicit.re


%check
%{_bindir}/R CMD check %{packname}


%files
%dir %{rlibdir}/%{packname}
%doc %{rlibdir}/%{packname}/html
%doc %{rlibdir}/%{packname}/CHANGELOG
%{rlibdir}/%{packname}/DESCRIPTION
%{rlibdir}/%{packname}/INDEX
%license %{rlibdir}/%{packname}/LICENSE
%{rlibdir}/%{packname}/NAMESPACE
%{rlibdir}/%{packname}/Meta
%{rlibdir}/%{packname}/R
%{rlibdir}/%{packname}/THANKS
%{rlibdir}/%{packname}/help
%dir %{rlibdir}/%{packname}/libs
%{rlibdir}/%{packname}/libs/%{packname}.so
%{rlibdir}/%{packname}/tests


%changelog
* Wed Jan 03 2024 Paul Thomas <paulthomas100199@gmail.com> - 2.3.8-1
- update to version 2.3.8

* Tue Jun 14 2022 misaka00251 <misaka00251@misakanet.cn> - 2.2.1-1
- Init package (Thanks to fedora team)
